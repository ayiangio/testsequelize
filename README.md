# Sequelize

[![npm version](https://badgen.net/npm/v/sequelize)](https://www.npmjs.com/package/sequelize)
[![Travis Build Status](https://badgen.net/travis/sequelize/sequelize?icon=travis)](https://travis-ci.org/sequelize/sequelize)
[![Appveyor Build Status](https://ci.appveyor.com/api/projects/status/9l1ypgwsp5ij46m3/branch/master?svg=true)](https://ci.appveyor.com/project/sushantdhiman/sequelize/branch/master)
[![codecov](https://badgen.net/codecov/c/github/sequelize/sequelize?icon=codecov)](https://codecov.io/gh/sequelize/sequelize)
[![npm downloads](https://badgen.net/npm/dm/sequelize)](https://www.npmjs.com/package/sequelize)
[![Merged PRs](https://badgen.net/github/merged-prs/sequelize/sequelize)](https://github.com/sequelize/sequelize)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

## Steps For Migrate and Seeders
1. Melakukan Inisialisasi Dan install dependensi nodejsnya Sequelize untuk project-nya
```
 $ npm install
 $ sequelize init

 ```
2. Edit file config/config.js menjadi seperti dibawah ini
```
{
  "development": {
    "username": "root", // username database
    "password": "admin", // password database
    "database": "sequelizetest", //nama database
    "host": "127.0.0.1",
    "dialect": "mysql",
    "operatorsAliases": false
  }
```

3. buat table dengan perintah
```
sequelize model:create --name Class --attributes className:string,instructor:string
```

4. Migrate model yang dibuat tadi dengan perintah
```
sequelize db:migrate
```
5. Kita buat untuk seeders dengan perintah
```
sequelize seed:generate --name demo-class
```
6. Sebelum menerapkan seednya kedalam tabel, kita edit file seeders/20200109135822-demo-class.js seperti dibawah ini
```
return queryInterface.bulkInsert('Classes', [{
        ClassName: 'Aerobik',
        instructor: 'Doe',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        ClassName: 'Zumba',
        instructor: 'Abram',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        ClassName: 'Keggle',
        instructor: 'Mirna',
        createdAt: new Date(),
        updatedAt: new Date()
      }
      ], {});
```
7. Terapkan Seednya dengan perintah ini
```
sequelize db:seed:all
```