'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Classes', [{
      ClassName: 'Aerobik',
      instructor: 'Doe',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      ClassName: 'Zumba',
      instructor: 'Abram',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      ClassName: 'Keggle',
      instructor: 'Mirna',
      createdAt: new Date(),
      updatedAt: new Date()
    }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
